################################################################################
#
# This Makefile creates the executable "fdmnes" of the X-ray absorption
# spectroscopy package.
#
# (GNUmake file, will not work with basic Unix make command, use gmake.)
#
# This Makefile can compile "fdmnes" for Linux (32 and 64 bit) and Windows PCs
# with CYGWIN. The versions for the different operating systems are stored in
# different subdirectories. For details, see below.
#
# Possible targets of this Makefile are:
#
# - all          (default) to make the executable
# - clean        to clean all object files created by the Makefile
# - allclean     to clean everything that was created by the Makefile
#
# Update: 30/03/2020 R. Wilcke (wilcke@esrf.fr)
#                    remove parts for Linux "g95" compiler (no longer used);
#                    modified the DBGFLAG options for "gfortran".
# Update: 16/03/2020 R. Wilcke (wilcke@esrf.fr)
#                    add "Ubuntu20.04" as OS, remove "Debian9";
#                    modify command to get MKLROOT ("modulecmd" gives slightly
#                    different answers on Ubuntu than on Debian);
#                    for Ubuntu, use system libraries for MUMPS, SCOTCH, METIS;
#                    add macro MKLLIB for the Intel MKL libraries;
#                    add new routine "RIXS.o" to macro FDM_SUBS.
# Update: 30/08/2019 R. Wilcke (wilcke@esrf.fr)
#                    add code for compilation on Debian 9.
# Update: 21/06/2019 R. Wilcke (wilcke@esrf.fr)
#                    put executables and object files in $(OSNAME)/bin instead
#                    of bin/$(OSNAME).
# Update: 04/04/2019 R. Wilcke (wilcke@esrf.fr)
#                    set MKLROOT for LINUX with "modulecmd" command for Intel
# Update: 08/11/2018 R. Wilcke (wilcke@esrf.fr)
#                    modify MPIHOME and add new macro MPILIB do get the MPI
#                    compiler and libraries from different directories for
#                    Debian 8 and Debian 9.
# Update: 09/04/2018 R. Wilcke (wilcke@esrf.fr)
#                    add new routine "diffraction.o" to macro FDM_SUBS.
# Update: 23/10/2017 R. Wilcke (wilcke@esrf.fr)
#                    modify (again) handling of "mpif.h" include file, remove
#                    macro MPI_INC
# Update: 02/05/2017 R. Wilcke (wilcke@esrf.fr)
#                    add again macro MPIHOME for the MPI installation directory.
# Update: 14/12/2016 R. Wilcke (wilcke@esrf.fr)
#                    replace "cp -p" command by "cp --preserve=timestamps".
# Update: 25/11/2016 R. Wilcke (wilcke@esrf.fr)
#                    add new routine "fdmx.o" to macro FDM_SUBS.
# Update: 13/06/2016 R. Wilcke (wilcke@esrf.fr)
#                    replace the shared libraries of MUMPS by the archive ones;
#                    remove code for Absoft and Portland compilers;
#                    remove "debian6" and add "debian8".
# Update: 12/02/2016 R. Wilcke (wilcke@esrf.fr)
#                    for the "clean" and "allclean" targets, also remove all *.o
#                    and *.mod files in the source directory (if there are any).
# Update: 26/08/2015 R. Wilcke (wilcke@esrf.fr)
#                    for CYGWIN use the MinGW package:
#                    - set macro SYSTEM to "MINGW"
#                    - set macro DIRNAM to "../bin/mingw"
#                    - use MinGW compiler "x86_64-w64-mingw32-gfortran.exe".
# Update: 21/08/2015 R. Wilcke (wilcke@esrf.fr)
#                    modify for use of MUMPS libraries:
#                    - use "gfortran" and "mpif90.openmpi" compilers instead of
#                      the ones from Intel, because the MUMPS package for Debian
#                      was compiled with those,
#                    - include the sequential or parallel MUMPS libraries for
#                      linking on LINUX,
#                    - use the FDMNES internal "mat_solve_gaussian.f90" instead
#                      of MUMPS for CYGWIN;
#                    modify the handling of the "mpif.h" include file;
#                    remove comments about CentOS, Suse and RedHat, add one for
#                    Debian7.
# Update: 13/10/2014 R. Wilcke (wilcke@esrf.fr)
#                    change extension of source files (except "sub_util.f") from
#                    ".f" to ".f90".
# Update: 02/10/2013 R. Wilcke (wilcke@esrf.fr)
#                    add new routine "optic.o" to macro FDM_SUBS.
# Update: 22/05/2013 R. Wilcke (wilcke@esrf.fr)
#                    remove routine "hubbard.o" from macro FDM_SUBS.
# Update: 11/06/2012 R. Wilcke (wilcke@esrf.fr)
#                    add a comment about the OS "Debian6".
# Update: 03/03/2011 R. Wilcke (wilcke@esrf.fr)
#                    removed the code for "Solaris";
#                    added compiler options for the "module" files directory.
# Update: 24/02/2011 R. Wilcke (wilcke@esrf.fr)
#                    create target "ERR_CONF" for incorrect specification of the
#                    executable;
#                    remove linker option "-static" for LINUX-MPI executable.
# Update: 13/12/2010 R. Wilcke (wilcke@esrf.fr)
#                    set flag "-no-ansi-alias" in FFLAGS for LINUX ifort.
# Update: 31/05/2010 R. Wilcke (wilcke@esrf.fr)
#                    define new macros FDM_SUBSPC and FFLAGSPC for routines that
#                    need special compile options;
#                    put module "sub_util.o" from FDM_SUBS into FDM_SUBSPC;
#                    set flag "-ffloat-store" in FFLAGSPC for CYGWIN;
#                    define new macro FDM_EXEC to contain the full name
#                    (including directory) of the FDMNES executable, and make
#                    this the main target ("all") of the Makefile.
# Update: 21/05/2010 R. Wilcke (wilcke@esrf.fr)
#                    add routines "hubbard.o" and "tddft.o" to macro FDM_SUBS.
# Update: 30/03/2009 R. Wilcke (wilcke@esrf.fr)
#                    add new routine "scf.o" to macro FDM_SUBS.
# Update: 25/04/2008 R. Wilcke (wilcke@esrf.fr)
#                    add new routines "fdm.o", "potential.o" and "select.o" to
#                    macro FDM_SUBS.
# Update: 03/08/2007 R. Wilcke (wilcke@esrf.fr)
#                    add new routine "tab_data.o" to macro FDM_SUBS;
#                    change OS dependent subdirectory name to "bin/cygwin" for
#                    CYGWIN (same name as provided by CS script "get_os").
# Update: 11/04/2007 R. Wilcke (wilcke@esrf.fr)
#                    change macro DIRECTORY to DIRNAM;
# Update: 11/01/2007 R. Wilcke (wilcke@esrf.fr)
#                    add new routine "lecture.o" to macro FDM_SUBS.
# Update: 09/01/2007 R. Wilcke (wilcke@esrf.fr)
#                    change logic for MPI compilation, remove MPIHOME macro.
# Update: 31/07/2006 R. Wilcke (wilcke@esrf.fr)
#                    make DIRECTORY a simple expanded variable and use shell().
# Update: 03/07/2006 R. Wilcke (wilcke@esrf.fr)
#                    get OS dependent subdirectory from CS script "get_os".
# Update: 28/06/2006 R. Wilcke (wilcke@esrf.fr)
#                    add macro MPIHOME for the MPI installation directory;
#                    change default for names of external references for ABSOFT.
# Update: 21/03/2005 R. Wilcke (wilcke@esrf.fr)
#                    change the name of some macros for better consistency with
#                    other software, simplify the conditional execution clauses.
# Update: 08/02/2005 R. Wilcke (wilcke@esrf.fr)
#                    first working version.
#
################################################################################
#
# Start of user-definable parameters
#-------------------------------------------------------------------------------
#

FDM_NAME = fdmnes_mpi
#FDM_NAME = fdmnes

FFLAGS = -c
DBGFLAG =
#DBGFLAG = -g
OPTLVL = 3
#OPTLVL = 0

#
# End of user-definable parameters
#-------------------------------------------------------------------------------
#
# FDM_SUBSPC contains the list of modules that need special compile options
# FFLAGSPC   contains these special compile options
FDM_SUBSPC =
FFLAGSPC =

#
# FDM_SUBMAT contains the Gaussian solver routines (with or without MUMPS).
#
FDM_SUBMAT = mat_solve_mumps.o

MPI_DUM = not_mpi.o

#
# Find out the operating system, and define the names of the directories
# for the object files and executables accordingly. They will be in
#    $(DIRNAM)/bin
#
# where $(DIRNAM) is
# - mingw        on PCs with the Cygnus user enviromnent
# - debian7      on 64-bit Linux systems with Debian version 7
# - debian8      on 64-bit Linux systems with Debian version 8
# - ubuntu20.04  on 64-bit Linux systems with Ubuntu version 20.04
#
HOST = $(shell uname -s)

ifeq ($(findstring CYGWIN,$(HOST)),CYGWIN)
   OSNAME := mingw
   SYSTEM = MINGW
else
   OSNAME := $(shell get_os)
   ifeq ($(word 1, $(HOST)),Linux)
      SYSTEM = LINUX
   endif      # Linux
endif      # CYGWIN
DIRNAM := ../$(OSNAME)/bin

#
# FDM_EXEC contains the full name of the executable, including directory name
# and (where applicable) suffix.
#
# Giving the full name of the executable as main target of the Makefile allows
# "make" to locate the executable. If only specifying the module name, "make"
# does not find it (as it is not in the present directory) and therefore always
# remakes it, even when that is not necessary.
#
FDM_EXEC := $(DIRNAM)/$(FDM_NAME)

ifeq ($(SYSTEM),LINUX)
#
# Intel Fortran 90 compiler.
#
# For external reference names use the ESRF's F90 defaults: convert names to
# lower case and add trailing underscore.
#
#   FC = ifort
#   MPIFC = mpiifort
#
# With OPTLVL=3 ifort needs this option, otherwise the program goes into an
# infinite loop (too agressive optimization by the compiler, it seems).
#
#   FFLAGS += -no-ansi-alias
#   FFLAGS += -I$(MKLROOT)/include
#
# The path to the module files is defined with the "-module" option for "ifort".
#
#   FFLAGS += -module $(DIRNAM)
#   DBGFLAG += -debug extended -traceback
#   DBGFLAG += -check all
#   DBGFLAG += -check uninit -check bounds
#   DBGFLAG += -debug all
#
# GNU Fortran 95 compiler
# Intel also has a "mpif90" compiler, thus specify here the "openmpi" version.
#
   ifeq ($(OSNAME),debian8)
      MPIHOME=/sware/devel_tools/packages/$(OSNAME)/openmpi/3.1.3
      MPILIB=$(MPIHOME)/lib
      INTELVER=2017
      MUMPSVER=MUMPS_5.1.2
      SCOTCHVER=scotch_6.0.6
      METISVER=metis-5.1.0
      MUMPSDIR=  /sware/exp/packages/$(OSNAME)/mumps/$(MUMPSVER)/lib
      SCOTCHDIR= /sware/exp/packages/$(OSNAME)/scotch/$(SCOTCHVER)/lib
      METISDIR=  /sware/exp/packages/$(OSNAME)/metis/$(METISVER)/lib
      FFLAGS += -I$(MUMPSDIR)/../include
   endif
   ifeq ($(OSNAME),ubuntu20.04)
      MPIHOME=/usr
      MPILIB=$(MPIHOME)/lib/x86_64-linux-gnu
#      INTELVER=2017
      INTELVER=2019
      MUMPSDIR=  /usr/lib/x86_64-linux-gnu
      SCOTCHDIR= /usr/lib/x86_64-linux-gnu
      METISDIR=  /usr/lib/x86_64-linux-gnu
      FFLAGS += -I/usr/include
   endif
   FC = gfortran
   MPIFC = $(MPIHOME)/bin/mpifort
#
# Use "modulecmd" to get the value of MKLROOT, the location of the Intel MKL
# libraries. Remove possibly trailing blanks from the string.
#
   MKLROOT = $(shell modulecmd sh show Intel/${INTELVER} 2>&1 | grep MKLROOT | \
      sed -e 's;[^/]*\(.*\)\([^ ]\) *;\1\2;')
#
# The path to the module files is defined with the "-J" option for "gfortran".
#
   FFLAGS += -J$(DIRNAM)
   FFLAGS += -m64 -fPIC -mcmodel=medium -I$(MKLROOT)/include
#   DBGFLAG += -ffpe-trap=invalid,zero,overflow
#   DBGFLAG += -finit-local-zero
#   DBGFLAG += -finit-real=nan
#   DBGFLAG += -finit-real=zero
#   DBGFLAG += -fcheck=all
#   DBGFLAG += -fcheck=bounds
#   DBGFLAG += -Wuninitialized
#
# Note that on the newer versions of the LINUX operating system, "static"
# linking does not work well with MPI. It seems that with static linking the
# MPI processes cannot communicate with each other if they are on different
# hosts.
#
   MPI_DUM =
   ifeq ($(FDM_NAME),fdmnes_mpi)
      FC = $(MPIFC)
      FFLAGS += -fopenmp
      LDFLAGS = -Wl,--no-as-needed -Wl,--export-dynamic -Wl,-rpath,$(MPILIB)
         ifeq ($(OSNAME),debian8)
            MUMPSLIB = -L$(MUMPSDIR) -ldmumps -lzmumps -lmumps_common -lpord \
               -L$(SCOTCHDIR) -lptscotchparmetis -lptesmumps -lptscotch \
               -lptscotcherr -lscotch -L$(METISDIR) -lmetis
         endif
         ifeq ($(OSNAME),ubuntu20.04)
            MUMPSLIB = -L$(MUMPSDIR) -ldmumps -lzmumps -lmumps_common -lpord \
               -L$(SCOTCHDIR) -lptscotchparmetis -lptesmumps -lptscotch \
               -lptscotcherr -lscotch -L$(METISDIR) -lmetis
         endif
      MKLLIB = $(MKLROOT)/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group\
         $(MKLROOT)/lib/intel64/libmkl_gf_lp64.a \
         $(MKLROOT)/lib/intel64/libmkl_gnu_thread.a \
         $(MKLROOT)/lib/intel64/libmkl_core.a \
         $(MKLROOT)/lib/intel64/libmkl_blacs_openmpi_lp64.a -Wl,--end-group \
	 -lgomp -lpthread -lm
      SYS_LIBS = -ldl -L$(MPILIB) -lmpi_mpifh -lmpi_usempif08 \
         -lmpi -lopen-rte -lopen-pal
   else
      FFLAGS += -I/usr/include/mumps_seq -I/usr/include
      MUMPSLIB = -ldmumps_seq -lzmumps_seq -lmumps_common_seq -lmpiseq_seq \
         -lesmumps -lpord_seq -lscotchmetis -lscotch -lscotcherr
      MKLLIB = -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_gf_lp64.a \
         $(MKLROOT)/lib/intel64/libmkl_sequential.a \
         $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group \
         -lpthread -lm -ldl
   endif
else
ifeq ($(SYSTEM),MINGW)
   ifeq ($(FDM_NAME),fdmnes)
#
# For CYGWIN, add suffix ".exe" to the name of the executable.
#
      FDM_EXEC := $(FDM_EXEC).exe
#
# The path to the module files is defined with the "-J" option for "gfortran".
#
      FC = x86_64-w64-mingw32-gfortran.exe
      FFLAGS += -J$(DIRNAM) -I/usr/include
#      DBGFLAG += -finit-local-zero -ffpe-trap=invalid,zero,overflow
#      DBGFLAG += -finit-real=nan
#      DBGFLAG += -finit-real=zero
#      DBGFLAG += -fcheck=all
#      DBGFLAG += -fcheck=bounds
#      DBGFLAG += -Wuninitialized
#
# Routine "DLAMC1()" in "sub_util.f" must be compiled with flag "-ffloat-store",
# otherwise the program goes into an infinite loop.
#
      FDM_SUBSPC = sub_util.o
      FFLAGSPC += -ffloat-store
#
# For CYGWIN use the internal Gaussian solver of FDMNES (no MUMPS).
      FDM_SUBMAT = mat_solve_gaussian.o
      LDFLAGS = -static
   else
      ERR_CONF = 1
   endif
endif      # CYGWIN
endif      # LINUX

FFLAGS += -O$(OPTLVL) $(DBGFLAG)

#
# Define search path for dependency files
#
vpath %.o ./$(DIRNAM)

#
# Define symbol for normal FDMNES files
#
FDM_SUBS = main.o RIXS.o clemf0.o coabs.o convolution.o diffraction.o dirac.o \
   fdm.o fdmx.o fprime.o fprime_data.o general.o lecture.o mat.o metric.o \
   minim.o optic.o potential.o scf.o selec.o spgroup.o sphere.o tab_data.o \
   tddft.o tensor.o $(FDM_SUBMAT) $(MPI_DUM)

#
# Define total system link symbol
#
LINK = $(MUMPSLIB) $(MKLLIB) $(SYS_LIBS)

all: $(FDM_EXEC)

$(FDM_EXEC): $(ERR_CONF) $(DIRNAM) $(FDM_SUBS) $(FDM_SUBSPC)
	(cd $(DIRNAM); \
	   $(FC) $(DBGFLAG) $(LDFLAGS) $(FDM_SUBS) $(FDM_SUBSPC) $(LINK) \
	   -o $(notdir $@))

$(ERR_CONF):
	$(error *** Cannot create $(FDM_NAME) on $(SYSTEM) - exit)

#
# Create directory for binaries if needed.
#
$(DIRNAM):
	mkdir -p $(DIRNAM)

#
# Rule to compile all normal Fortran source files.
#
$(FDM_SUBS): %.o: %.f90
	$(FC) $(FFLAGS) $< -o ./$(DIRNAM)/$(notdir $@)

#
# Rule to compile all Fortran source files needing special compile options.
#
$(FDM_SUBSPC): %.o: %.f
	$(FC) $(FFLAGS) $(FFLAGSPC) $< -o ./$(DIRNAM)/$(notdir $@)

#
# Remove all object files
#
clean:
	rm -f $(DIRNAM)/*.o $(DIRNAM)/*.mod *.o *.mod

#
# Remove everything created by the Makefile
#
allclean:
	rm -rf $(DIRNAM) *.o *.mod
