#!/bin/bash -l
################################################################################
#
# Example of a command file to submit FDMNES jobs to the SLURM batch queue.
#
# The FDMNES program calculates X-ray spectra over a user-defined range of
# energies. There is a serial and a parallel version available. The parallel
# version will automatically be selected if the user requests more than one
# process to run when submitting the job to the SLURM batch queue.
#
# FDMNES has two levels of parallelisation. The main one is over the energies,
# and this scales well with the number of cores assigned to the job. The second
# one is a sparse matrix calculation that can be parallelised for every energy
# calculation. This scales well only up to about 4 parallel processes per
# energy.
#
# If the FDMNES script that is submitted by this command file is called without
# arguments concerning the parallel processes, the default is that the matrix
# calculation is not parallelised and that the number of cores requested from
# the SLURM scheduler is the number of parallel processes for the energy.
#
# FDMNES calculations may need large amounts of memory, and assigning that many
# parallel processes to the nodes can cause the job to be terminated for
# insufficient memory. It is possible to limit the number of parallel energy
# processes with the "-np" parameter to the script.
#
# If only that parameter is specified for the calculation, there is still no
# parallelisation for the matrix calculations. This can be enabled by setting
# the environment variable HOST_NUM_FOR_MUMPS. The value of this variable gives
# the number of parallel matrix calculations for each energy point. The default
# is 1, i.e. no parallelisation for the matrix calculations.
#
# For a given job, the total number of parallel processes is thus
#    ntot = (# of energy processes) * HOST_NUM_FOR_MUMPS
# where the number of energy processes is specified by the "-np" parameter.
#
# For more details, see the script "fdmnes.sh", the file "Readme_ESRF.txt" in
# the FDMNES home directory at the ESRF, and the documentation in the FDMNES
# distribution.
#
# The batch jobs are submitted to SLURM with the "sbatch" command. Each "sbatch"
# command submits one job. If several jobs are to be submitted, just give the
# corresponding number of "sbatch" commands (see examples below).
#
# The "sbatch" command can take many options (for details, see "man sbatch" or
# the SLURM documentation in https://slurm.schedmd.com/man_index.html).
#
# The ones most useful for FDMNES are:
# - the number of parallel processes to run (default: 1);
# - the maximum (elapsed) run time (default: 2 hours);
# - the directories where the job starts (default: the present directory).
#
# Another option that can be useful is the memory available (for jobs that need
# large quantities of memory).
#
# The number of parallel processes and the maximum run time are specified with
# the "sbatch" options "ntasks" and "time" in the form
#    --ntasks=nproc --time=nh:nm:ns
# with
#    nproc: the number parallel processes
#    nh   : the number of hours to run
#    nm   : the number of minutes to run
#    ns   : the number of seconds to run
#
# Example: --ntasks=4 --time=1:20:00
#    requests 4 parallel tasks and a total elapsed time of 1 hour 20 minutes.
#
# If one wants to submit more than one job with this command file, they have to
# be launched in different directories, as FDMNES will only accept the fixed
# name "fdmfile.txt" as its input file name. The "sbatch" option "chdir" allows
# to specify for each job a directory (with path!) where the job will be
# started.
#
# Example: --chdir=/users/dupont/data/exp1234
#    starts the FDMNES batch job in the directory "/users/dupont/data/exp1234".
#
# Memory requirements can be specified per node or per core with the options
# "mem" or "mem-per-cpu". If no memory option is given, each process gets as
# default the amount (memory of the node) / (number of cores of the node).
# The special value "--mem=0" gives the job access to all memory on each node.
#
# Example: --mem=258000
#    starts the FDMNES batch job on nodes with at least 250000 MB
#
# One can give input arguments (like the number of parallel energy processes) to
# the executable.
#
# Example:
#    sbatch --nodes=8 --mincpus=28 --mem=0 --exclusive --time=72:0:0 \
#    $EXECUT -np 32
#
#    starts the FDMNES batch job on 8 nodes with 28 cores each with a total
#    elapsed time of 72 hours. The executable is given the additional input
#    argument "-np 32", which means that only 32 parallel energy loops will be
#    started: 4 per node, which leaves 24 cores per node idle. This can be used
#    to give more memory to each energy process.
#
# If one wants to parallelise the sparse matrix calculation for each energy
# point, the environment variable HOST_NUM_FOR_MUMPS has to be set to a value
# different than 1. To do this, it should be specified on the command line with
# the "sbatch" command
#
# Example:
#    export HOST_NUM_FOR_MUMPS=4; sbatch --nodes=3 --mincpus=28 --mem=0 \
#    --exclusive --time=72:0:0 $EXECUT
#
#    starts the FDMNES batch job on 3 nodes with 28 cores each with a total
#    elapsed time of 72 hours. 84 parallel processes will be started (3 nodes
#    with 28 cores each), but each energy process executes the matrix
#    calculation on 4 parallel processes as requested by the HOST_NUM_FOR MUMPS
#    environment variable. Thus there will be only 21 parallel energy processes
#    running (7 per node).
#
# The specification for the number of parallel energy processes and the one for
# the number of parallel matrix calculations can both be given for the same job.
#
# Example:
#    export HOST_NUM_FOR_MUMPS=4; sbatch --nodes=3 --mincpus=28 --mem=0 \
#    --exclusive --time=72:0:0 $EXECUT -np 6
#
#    starts the FDMNES batch job on 3 nodes with 28 cores each with a total
#    elapsed time of 72 hours. 6 parallel energy processes will be started as
#    specified by the "-np" parameter (2 on each node), but each energy process
#    executes the matrix calculation on 4 parallel processes as requested by the
#    HOST_NUM_FOR MUMPS environment variable. Thus there will be in total 24  
#    parallel processes running (8 per node).
#
################################################################################
# Created:     06/04/2011 R. Wilcke (wilcke@esrf.fr)
#
# Modified:    20/04/2020 R. Wilcke (wilcke@esrf.fr)
#              changed partition request from "fast-io" to "nice".
# Modified:    16/01/2020 R. Wilcke (wilcke@esrf.fr)
#              added variable FDMVERS containing the desired version of FDMNES.
# Modified:    18/09/2019 R. Wilcke (wilcke@esrf.fr)
#              modify for use with SLURM workload manager.
# Modified:    26/06/2019 R. Wilcke (wilcke@esrf.fr)
#              get directory with script "fdmnes.sh" with "module" command;
#              use "bash login shell" (/bin/bash -l), needed for "module";
#              exit with error if script not found;
#              remove variable FDMVERS.
# Modified:    20/06/2019 R. Wilcke (wilcke@esrf.fr)
#              added variable FDMVERS to define the FDMNES version.
# Modified:    05/12/2018 R. Wilcke (wilcke@esrf.fr)
#              added "Example 5" that uses input arguments and the environment
#              variable HOST_NUM_FOR_MUMPS with the executable.
# Language:    Bourne shell script with indented text
# Package:     FDMNES
# Status:      Operational
#
# (C) Copyright 2011, DAU Unit, Software Group, ESRF, all rights reserved.
################################################################################

#
# Define the version of FDMNES to be used (form: yyyymmdd) and the directory
# containing the script "fdmnes.sh".
#
# Normally this directory is put in the PATH variable with the "module load"
# command, and the variable FDMHOME stays empty. If a different version of
# FDMNES is required, the corresponding "fdmnes.sh" script can be selected here.
#
FDMVERS=20200831
#FDMHOME=/scisoft/users/wilcke/swdev/fdmnes/fdmnes_$FDMVERS/ubuntu20.04/bin

if [ X$FDMHOME = X ]
then
   FDMHOME=/sware/exp/packages/ubuntu20.04/fdmnes/$FDMVERS/bin
fi
FDMHOME=${FDMHOME}/

#
# The FDMNES jobs are started with the interface script "fdmnes.sh".
# Exit with error message if the script is not found.
#
EXECUT=`which ${FDMHOME}fdmnes.sh`
if [ $? -ne 0 ]
then
   echo "ERROR: script \"fdmnes.sh\" not found"
   exit 1
fi

#
# All examples are commented out. To select one, remove the comment symbols "#"
# on the corresponding "sbatch" line(s).
#

#
# Example 1: submit one non-parallel job with 30 minutes maximum runtime.
#
# No "ntasks" option is given, thus the default "1 task" is taken.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#sbatch --time=0:30:0 $EXECUT

#
# Example 2: submit one parallel job with 8 processes for 2 hours maximum
# runtime.
#
# No "time" option is given, thus the default "2 hours" is taken.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#sbatch --ntasks=8 $EXECUT

#
# Example 3: submit two parallel jobs each with 5 processes for 1 hour 30
# minutes maximum runtime.
#
# Each job will be started in its own directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#sbatch --chdir=/users/johndoe/example_1 --ntasks=5 --time=1:30:0 $EXECUT
#sbatch --chdir=/users/johndoe/example_2 --ntasks=5 --time=1:30:0 $EXECUT

#
# Example 4: submit one parallel job to 4 nodes with 1 hour maximum runtime.
# Each node should run 4 processes. The nodes must have (at least) 94 000 MB
# memory each.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#sbatch --nodes=4 --ntasks-per-node=4 --mem=94000 --time=1:0:0 $EXECUT

#
# Example 5: submit one parallel job to 3 nodes with 28 cores each for 72 hours
# maximum runtime. For this job, both the number of parallel energy processes
# and the number of parallel matrix calculations are specified.
#
# The executable contained in $EXECUT is given the additional input argument
# "-np 6" to specify that the job should be executed by 6 parallel processes for
# the energy loop; this means that there will only be 2 processes for the energy
# loop per node.
#
# However, each of those energy loops will use 4 parallel processes for the
# matrix calculation. That is specified by the HOST_NUM_FOR_MUMPS environment
# variable. Thus there will be 8 parallel processes per node.
#
# The total number of parallel processes for the SLURM job is therefore 24.
#
# The job has exclusive use of the 3 allocated nodes and access to all memory
# available on each node (with the "--exclusive" and the "--mem=0" options).
#
# In order to obtain nodes with 28 cores, the job must select the "nice"
# partition. This is done with the "-p nice" option.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#export HOST_NUM_FOR_MUMPS=4; sbatch -p nice --nodes=3 --mincpus=28 --mem=0 \
#   --exclusive --time=72:0:0 $EXECUT -np 6
export HOST_NUM_FOR_MUMPS=4; sbatch -p nice --nodes=2 --mincpus=8  \
   --time=1:0:0 $EXECUT -np 4
